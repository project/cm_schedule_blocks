<?php
/**
 * @file
 * cm_schedule_blocks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cm_schedule_blocks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cm_schedule_blocks_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cm_schedule_blocks_node_info() {
  $items = array(
    'cm_schedule_block' => array(
      'name' => t('CM Schedule block'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
