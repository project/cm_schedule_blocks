<?php
/**
 * @file
 * CM Schedule Blocks module, code for create and delete arings.
 */

/**
 * Get a node.
 */
function _cmsb_get_node($nid, $type) {
  $node = node_load($nid);
  if (FALSE === $node) {
    return FALSE;
  }
  if ($type !== $node->type) {
    return FALSE;
  }
  return $node;
}

/**
 * Help function.
 */
function _cmsb_length($nid) {
  if (FALSE === $node = _cmsb_get_node($nid, 'cm_schedule_block')) {
    return FALSE;
  }
  $block_length = array('block_length' => 0);
  if (!empty($node->field_cmsb_broadcasts[LANGUAGE_NONE]) && !empty($node->field_cmsb_cm_shows[LANGUAGE_NONE])) {
    $shows = $node->field_cmsb_cm_shows[LANGUAGE_NONE];
    $round_up = 0;
    if (!empty($node->field_cmsb_round_up_to[LANGUAGE_NONE][0]['value'])) {
      $round_up = $node->field_cmsb_round_up_to[LANGUAGE_NONE][0]['value'];
    }
    $block_length = array();
    $total_length[] = array();
    $offset = 0;
    foreach ($shows as $showid) {
      $showid = $showid['target_id'];
      if (FALSE === $show = _cmsb_get_node($showid, 'cm_show')) {
        drupal_set_message(t('Error loading cm_show in _cmsb_length'), 'error');
        return FALSE;
      }
      $show_length = 0;
      drupal_alter('cm_schedule_blocks_duration', $show_length, $show);
      $block_length['cm_show'][$showid]['cm_show_orgiginal_lenght'] = $show_length;

      $channel = FALSE;
      if (!empty($node->field_cmsb_channel[LANGUAGE_NONE][0]['tid'])) {
        $channel = $node->field_cmsb_channel[LANGUAGE_NONE][0]['tid'];
      }
      $block_length['cm_show'][$showid]['field_cm_show_channel'] = $channel;

      $block_length['cm_show'][$showid]['cm_show_nid'] = $showid;
      $block_length['cm_show'][$showid]['cm_show_title'] = $show->title;
      $block_length['cm_show'][$showid]['cm_show_offset'] = $offset;

      if ((0 !== $round_up) && (0 !== $show_length % $round_up)) {
        $show_length = $round_up + $round_up * (int)($show_length / $round_up);
      }
      $block_length['cm_show'][$showid]['cm_show_rounded_lenth'] = $show_length;
      $total_length[] = $show_length;
      $offset += $show_length;
    }
    $block_length['block_length'] = array_sum($total_length);
  }
  return $block_length;
}

/**
 * Delete airings marked for deletion.
 */
function _cmsb_delete_block_airings($nid) {
  if (FALSE === $node = _cmsb_get_node($nid, 'cm_schedule_block')) {
    return FALSE;
  }
  if (!empty($node->field_cmsb_airings[LANGUAGE_NONE])) {
    $count_airings = count($node->field_cmsb_airings[LANGUAGE_NONE]);
    foreach ($node->field_cmsb_airings[LANGUAGE_NONE] as $airing) {
      entity_delete('airing', $airing['target_id']);
    }
    unset($node->field_cmsb_airings[LANGUAGE_NONE]);
    node_save($node);
    drupal_set_message($count_airings . ' Airings deleted');
  }
}

/**
 * Create airings.
 */
function _cmsb_create_block_airings($nid) {
  if (FALSE === $node = _cmsb_get_node($nid, 'cm_schedule_block')) {
    return FALSE;
  }
  $data_time_array = FALSE;
  // Check if cmsb_template exist.
  if (!empty($node->field_cmsb_template[LANGUAGE_NONE][0]['target_id'])) {
    $cmsb_template = entity_load_single('cmsb_template', $node->field_cmsb_template[LANGUAGE_NONE][0]['target_id']);
    if (!empty($cmsb_template->field_cmsb_template_offset[LANGUAGE_NONE])) {
      foreach ($cmsb_template->field_cmsb_template_offset[LANGUAGE_NONE] as $offset) {
        // Create starttime for airing in UTC time from offset in user timezone.
        $date = new DateTime($node->field_cmsb_template_start_date[LANGUAGE_NONE][0]['value'], new DateTimeZone(drupal_get_user_timezone()));
        $date->setTimezone(new DateTimeZone('UTC'));
        $date->modify('+' . $offset['value'] . ' sec');
        $data_time_array[]['value'] = $date->format('Y-m-d H:i:s');
      }
    }
  }
  elseif (!empty($node->field_cmsb_broadcasts[LANGUAGE_NONE])) {
    $data_time_array = $node->field_cmsb_broadcasts[LANGUAGE_NONE];
  }
  if ($data_time_array) {
    $cmsb = _cmsb_length($nid);
    foreach ($data_time_array as $broadcast) {
      $airing_time = $broadcast;
      foreach ($cmsb['cm_show'] as $show) {
        $date = new DateTime($broadcast['value']);
        $date->modify('+' . $show['cm_show_offset'] . ' sec');
        $airing_time['value'] = $date->format('Y-m-d H:i:s');
        $date = new DateTime($airing_time['value']);
        $date->modify('+' . $show['cm_show_orgiginal_lenght'] . ' sec');
        $airing_time['value2'] = $date->format('Y-m-d H:i:s');
        if (FALSE === $show['field_cm_show_channel']) {
          $field_airing_channel = array();
        }
        else {
          $field_airing_channel = array(LANGUAGE_NONE => array( array('tid' => $show['field_cm_show_channel'])));
        }
        $values = array(
          'type' => 'airing',
          'field_airing_title' => array(LANGUAGE_NONE => array( array(
                'value' => $show['cm_show_title'],
                'format' => NULL,
                'safe_value' => strip_tags($show['cm_show_title']),
              ))),
          'field_airing_duration' => array(LANGUAGE_NONE => array( array('value' => $show['cm_show_orgiginal_lenght']))),
          'field_airing_channel' => $field_airing_channel,
          'field_airing_date' => array(LANGUAGE_NONE => array($airing_time)),
          'field_airing_show_ref' => array(LANGUAGE_NONE => array( array('target_id' => $show['cm_show_nid']))),
        );
        $airing = entity_create('airing', $values);
        $airing->save();
        $block_airings[] = array('target_id' => $airing->airing_id);
        drupal_set_message(t('Airgin: @cm_show_title, @airing_time1 (UTC) to @airing_time2 (UTC) created', array(
          '@cm_show_title' => $show['cm_show_title'],
          '@airing_time1' => $airing_time['value'],
          '@airing_time2' => $airing_time['value2'],
        )));
      }
    }
    // Attach arings to cmsb cm_schedule_block.
    $node->field_cmsb_airings[LANGUAGE_NONE] = $block_airings;
    node_save($node);
  }
}
