CONTENTS OF THIS FILE
---------------------

Introduction

You can create blocks of cm_show in two ways
1. each block can have a start date and a template with multiple offsets from startdate
2. each block can have multiple date_time when the block will start broadcasting.
You can create cm_airing through this block with method 1 or 2.

Each block keeps track of which cm_airings it has created, it makes it easy to remove all airings created by this particular block.

This module will make it much easier to create and keep track of cm_airing.

Requirements:
- cm_airing
- cm_show
- entityreference_view_widget
- entity

Installation
- Enable the module

Uninstallation
- Diable the module, uninstall the module
  - all blocks and templates are deleted, all airings created by module stays

Maintainers
joakimforgren
kristofferwiklund
