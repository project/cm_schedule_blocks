<?php
/**
 * @file
 * CM Schedule Blocks module, code for cmsb_template.
 */

/**
 * Implements hook_permission().
 */
function cm_schedule_blocks_permission() {
  return array('admin block_template' => array(
      'title' => t('Administer CM Schedule Block Template'),
      'description' => t('Perform administration on CM Schedule Block Template.'),
    ), );
}

/**
 * Implements hook_access().
 * - workaround for https://www.drupal.org/node/2153463.
 */
function cm_schedule_blocks_template_access($op, $node, $node) {
  if ($op == 'view') {
    return TRUE;
  }
  return user_access('admin block_template');
}

/**
 * Implements hook_load().
 */
function cmsb_template_load($template_id) {
  $template = entity_load_single('cmsb_template', $template_id);
  return $template;
}

/**
 * Form add cmsb_template.
 */
function cm_schedule_block_template_add() {
  $template = entity_create('cmsb_template', array());
  return drupal_get_form('cm_schedule_block_template_form', $template);
}

/**
 * Form cmsb_template.
 */
function cm_schedule_block_template_form($form, &$form_state, $template = NULL) {
  if (!isset($template->is_new)) {
    drupal_set_title($template->label());
  }
  $form_state['cmsb_template'] = $template;
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#default_value' => !empty($template->label) ? $template->label : '',
    '#weight' => 1,
  );

  field_attach_form('cmsb_template', $template, $form, $form_state);
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 50,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save template'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Form validate cmsb_template.
 */
function cm_schedule_block_template_form_validate($form, &$form_state) {
  entity_form_field_validate('cmsb_template', $form, $form_state);
}

/**
 * Form validate cmsb_template.
 */
function cm_schedule_block_template_form_submit($form, &$form_state) {
  entity_form_submit_build_entity('cmsb_template', $form_state['cmsb_template'], $form, $form_state);
  $template = $form_state['cmsb_template'];
  $template->save();
  drupal_set_message(t('The changes have been saved.'));
  $form_state['redirect'] = 'admin/structure/cm-schedule-block-template';
}

/**
 * Delete confirmation form.
 */
function cm_schedule_block_template_delete($form, &$form_state, $template) {
  $form_state['cmsb_template'] = $template;
  return confirm_form($form, t('Are you sure you want to delete template: %title?', array('%title' => $template->label())), 'admin/structure/cm-schedule-block-template', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Delete form submit handler.
 */
function cm_schedule_block_template_delete_submit($form, &$form_state) {
  $template = $form_state['cmsb_template'];
  entity_delete('cmsb_template', $template->id);
  drupal_set_message(t('The template have been deleted.'));
  $form_state['redirect'] = 'admin/structure/cm-schedule-block-template';
}
